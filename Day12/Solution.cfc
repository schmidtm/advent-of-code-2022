component {
	CR = "
";
	FS = chr(27);
	INFINITY = "";
	function runSample()
	{
		runPart1("SampleInput.txt");
		WriteOutput(CR);
   //      runPart2("SampleInput.txt");
	}

	function runRealWorld()
	{
	//   runPart1("PuzzleInput.txt");
	   WriteOutput(CR);
	  //  runPart2("PuzzleInput.txt");
		
	}
	function init()
	{
		INFINITY = CreateObject("java", "java.lang.Double").POSITIVE_INFINITY
	}

 
	function createGrid(input)
	{
		
		var valA = asc('a');
		var valZ = asc('z');
		var valCapS = asc("S");
		var valCapE = asc('E');

		grid = ArraySlice(input.split("\R"),1)
			.map((m) => ArraySlice(m.split(""), 1)
				.map( (m) => { 
					
					var valM = asc(m);
					return {
						Height: valM  ==  valCapS ? valA : valM == valCapE ? valZ : valM, 
						Val:m, 
						visited: false, 
						minLen:INFINITY,
						BestPath:"",
						Reference:""
						}
					}
				)
			);


		var retGraph = {graph: {}, start:"", end: ""};
		/* I can be visited by my neighbor if I am either a lower elevation then them or they are one higher then me */
		for ( var j = 1; j <= grid.len(); j++)
		{
			row = grid[j];
			for ( var k = 1; k <= row.len(); k++ ) 
			{
				row[k].Reference = "#j#,#k#"
				neighbors = [];
				var myHeight = row[k].Height;
				var arrLooksie = [[0, +1], [1, 0], [-1, 0], [0, -1]];
				for ( var lookAtMe in arrLooksie )
				{
					var potJ = lookAtMe[1]+j
					var potK = lookAtMe[2]+k
					if (  potJ >= 1  
						&&  potK >= 1
						&& potJ <= grid.len()
						&& potK <= row.len() )
					{						
						var potHeight = grid[potJ][potK].Height
						var isPot=false;
						if ( potHeight >= myHeight -1   )
						{							
							isPot = true;
						} 
						if ( isPot )
							neighbors.push( "#potJ#,#potK#");
					}
				}

				if ( asc(grid[j][k].Val) == valCapS  )
				{
					retGraph["start"]="#j#,#k#";
				}

				
				if ( asc(grid[j][k].Val) == valCapE  )
				{
					retGraph["end"]="#j#,#k#";
				}
				retGraph.graph["#j#,#k#"] = grid[j][k];
				retGraph.graph["#j#,#k#"].neighbors = neighbors;
				
			}
		}
		return retGraph;
	}
function dumpJSON(itm)
{
	WriteOutput(CR & (SerializeJSON(itm)) & CR);
}
function justDoit(graph, start, end) {
   graph[end].minLen = 0;
   graph[end].visited = true;
   var queue = [end];
   while ( queue.len() > 0 ) {
	var pos = queue.shift();
	var itm = graph[pos];
	dumpJSON(pos);
		for (var neighbor in itm.neighbors )
		{
			var neighborItm = graph[neighbor]			
			var shortestDist =  neighborItm.minLen+1;
			var minDist =  min(shortestDist, itm.minLen);
			
			if ( itm.minLen != minDist )
			{
				itm.minLen =  minDist;
				itm.bestPath = neighborItm.bestPath & "<--" &  neighbor;
			}

			if ( ! neighborItm.visited )
			{
				dumpJSON(neighborItm);
				queue.push(neighbor);
				neighborItm.visited = true;
			}
		}
		dumpJSON([itm]);
   }

   //dumpJSON(graph);
   return graph[start].minLen;

}

	
	function runPart1(filename) {
		var data = FileRead(filename);        
		
		var grid = createGrid(data);
	   var res = justDoit(grid.graph, grid.start, grid.end);

	   WriteOutput(res);
	   dumpJSON(grid.graph[grid.start]);
	   dumpJSON(grid.graph);
	   
	}

  
   

	function runPart2(filename)
	{   
		var data = FileRead(filename);        
		var dataArr = ListToArray(data, CR);
		var Rows = parseColRows(data)
		var res = calcScenicScore(Rows);
		WriteOutput(res);
	}

  
		
 
	
}