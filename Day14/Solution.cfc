component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
     //    runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
      // runPart1("PuzzleInput.txt");
       WriteOutput(CR);
      //  runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }

    function isList(pair)
    {
        return left(pair, 1) == '[';
    }
    function trimBrackets(pair) {
        if (isList(pair) )
        {
            pair = mid(pair, 2, len(pair)-2);            
        }
        return pair;
    }
   

    function runPart1(filename) {
        var data = FileRead(filename);       
        var topSand = {X:"500", y:"0"}
        var rockFormations = ListToArray(data, CR, true, true);
        
        rockFOrmations = rockFormations.map( (formation) => 
            {
                return formation.ListToArray(" -> ", false, true).map(
                    (coord) => 
                        { var itms = coord.ListToArray(",");
                            return {x:itms[1], y:itms[2]};
                        });
            }
        )
          
        var minV = duplicate(topSand);
        var maxV = duplicate(topSand);
        

        rockFormations.each( (rock)=>
            rock.each( (coord) => {
                minV.x = min(minV.x, coord.x);
                minV.y = min(minV.y, coord.y);
                maxV.x = max(maxV.x, coord.x);
                maxV.y = max(maxV.y, coord.y);
            })
        )

        dumpJSON(rockFormations);
        dumpJSON([minV, maxV])

    }
    function dumpJSON(itm)
    {
        WriteOutput(CR & (SerializeJSON(itm)) & CR);
    }
    

    function runPart2(filename)
    {   
        var data = FileRead(filename);        
        data = data & "#CR#[[2]]#CR#[[6]]"
      
        
    }

  
        
 
    
}