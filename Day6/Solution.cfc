component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
         runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
          runPart1("PuzzleInput.txt");
        WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }
    function runPart1(filename) {
        var data = FileRead(filename);        
        var dataArr = ListToArray(data, "");        
        for ( var i = 4; i lte ArrayLen(dataArr); i++)
        {
            if ( dataArr[i-3] != dataArr[i-2] && dataArr[i-3] != dataArr[i-1] && dataArr[i-3] != dataArr[i]
                && dataArr[i-2] != dataArr[i-1] && dataArr[i-2] != dataArr[i]
                && dataArr[i-1] != dataArr[i])
            {
                WriteOutput(i);
                break;
            }
        }
        
       
    }

  
   

    function runPart2(filename)
    {   
        var data = FileRead(filename);        
        var dataArr = ListToArray(data, "");        
        for ( var i = 14; i lte ArrayLen(dataArr); i++)
        {
            var match = false;
            for ( var j = 13; j >= 0; j--)
            {
                for ( var k = j-1; k >= 0; k-- )
                {
                    if ( dataArr[i-j] == dataArr[i-k])
                    {
                        match = true;
                        break;
                    }
                }
                if ( match )
                    break;
            }



            if ( ! match )
            {
                WriteOutput(i);
                break;
            }
            
        }
    }

  
        
 
    
}