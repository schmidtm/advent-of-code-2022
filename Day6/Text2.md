## --- Part Two ---

Your device's communication system is correctly detecting packets, but still isn't working. It looks like it also needs to look for *messages* .

A *start-of-message marker* is just like a start-of-packet marker, except it consists of *14 distinct characters* rather than 4.

Here are the first positions of start-of-message markers for all of the above examples:

* `mjqjpqmgbljsphdztnvjfqwrcgsmlb`: first marker after character `<em>19</em>`
* `bvwbjplbgvbhsrlpgdmjqwftvncz`: first marker after character `<em>23</em>`
* `nppdvjthqldpwncqszvftbrmjlhg`: first marker after character `<em>23</em>`
* `nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg`: first marker after character `<em>29</em>`
* `zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw`: first marker after character `<em>26</em>`

*How many characters need to be processed before the first start-of-message marker is detected?*
