CR = "\n"
FS = chr(27)

def run_sample() :
    runPart1("SampleInput.txt");
    print("\n")
    runPart2("SampleInput.txt");

def run_realWorld() :
    runPart1("PuzzleInput.txt");
    print("\n")
    runPart2("PuzzleInput.txt");
   
def runForXSize(data, size) :
    i = size
    while i <= len(data) :
        match = False
        j = size - 1
        while j >= 0 :
            k = j - 1
            while k >= 0 :
                if data[i-j] == data[i-k] :
                    match = True
                    break                
                k-=1
            if match :
                break
            j-=1
        if  match != True :
            print(i+1)
            break
        i+=1
    

def runPart1(fileIn) :
    f = open(fileIn, "r")
    data = f.read()    
    runForXSize(data, 4)

def runPart2(fileIn) :
    f = open(fileIn, "r")
    data = f.read()    
    runForXSize(data, 14)
    
    


print("Sample" + CR)
run_sample()
print (CR + "RealWorld" + CR)
run_realWorld()