CR = "\n"

def run_sample() :
    runPart1("SampleInput.txt");
    print("\n")
    runPart2("SampleInput.txt");

def run_realWorld() :
    runPart1("PuzzleInput.txt");
    print("\n")
    runPart2("PuzzleInput.txt");

def determineLetterValue(letterIn) :
    if  letterIn.isupper() :
        return ord(letterIn) - ord('A')+27
    return ord(letterIn) - ord('a')+1

def divideSackInHalf(sackIn) :
    halfLen = int(len(sackIn) / 2)
    backStop = int(-1 * halfLen)
    return {
    'secondSack' : sackIn[backStop :  ], 
    'firstSack' : sackIn[0: halfLen] }

def calcPrioity(twoSacks) :
    sum = 0
    sack = divideSackInHalf(twoSacks)
    sack1 = sack["firstSack"]
    sack2 = sack["secondSack"]
    while len(sack1) :
        charWeCare = sack1[0]
        lenSecSack = len(sack2)
        sack2 = sack2.replace(charWeCare, "")
        sack1 = sack1.replace(charWeCare, "")
        if ( len(sack2) != lenSecSack ) :
            sum += determineLetterValue(charWeCare)
    return sum

def calcPart2Priority(sack1, sack2, sack3) :
    sum = 0
    while len(sack1) :
        charWeCare = sack1[0]
        len2Sack = len(sack2)
        len3Sack = len(sack3)
        sack2 = sack2.replace(charWeCare, "")
        sack3 = sack3.replace(charWeCare, "")
        sack1 = sack1.replace(charWeCare, "")
        if len(sack2) != len2Sack and len(sack3) != len3Sack :
            sum += determineLetterValue(charWeCare)
            break

    return sum

def runPart1(fileIn) :
    f = open(fileIn, "r")
    data = f.read()
    sum = 0
    arr = data.split(CR);
    for i in arr :
        sum += calcPrioity(i)
    
    print(sum)

def runPart2(fileIn) :
    f = open(fileIn, "r")
    data = f.read()
    sum = 0
    arr = data.split(CR);
    i=0
    while (  i < len(arr)) :    
        sum += calcPart2Priority(arr[i], arr[i+1], arr[i+2])
        i+=3
    print(sum)

def totalCalories(input) :
    vals = input.split(CR)
    tot = 0
    for i in vals :
        tot = tot + int(i)
    return tot

def normalizeData(dataIn) :
    dataIn = dataIn.replace("\r\n", CR);
    dataIn = dataIn.replace("\n\r", CR);
    
    dataIn = dataIn.replace(" ", "")
    dataIn = dataIn.replace(CR+CR, "|")
    dataOut = dataIn.split("|")
    return dataOut

print("Sample" + CR)
run_sample()
print (CR + "RealWorld" + CR)
run_realWorld()