component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
        runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
        runPart1("PuzzleInput.txt");
        WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }



    function determineLetterValue(letterIn)
    {
        if ( ReFind("[A-Z]", letterIn) > 0 )
            return asc(letterIn) - asc('A')+27;
        return asc(letterIn) - asc('a')+1;
    }

    function divideSackInHalf(twoSacks) {
        var retStr = {};
        retStr.SecondSack = right(twoSacks, Len(twoSacks)/2);
        retstr.FirstSack = left(twoSacks, len(twoSacks)/2);
        return retStr;
    }

    function calcPriority(twoSacks) {
        var sack = divideSackInHalf(twoSacks);
        var sum = 0;
        while ( Len(sack.FIrstSack) )
        {
            var charWeCare = Left(sack.FirstSack, 1);
            var lenSecSack = Len(sack.SecondSack);
            sack.SEcondSack = Replace(sack.SEcondSack, charWeCare, "", "ALL");
            sack.FirstSack = Replace(sack.FirstSack, charWeCare, "", "ALL");
            if ( len(sack.SecondSack) < lenSecSack) 
            {
                sum += determineLetterValue(charWeCare);
            }
        }
        return sum;
    }

    function calcPart2Priority(sack1, sack2, sack3) {
        var sum=0;
        while ( len(sack1) )
        {
            var charWeCare = Left(sack1, 1);
            var len2Sack = len(sack2);
            var len3Sack = len(sack3);
            sack2 = replace(sack2, charWeCare, "", "ALL");
            sack3 = replace(sack3, charWeCare, "", "ALL");            
            sack1 = replace(sack1, charWeCare, "", "ALL");
            if ( len2Sack > len(sack2) && len3Sack > len(sack3))
            {
                sum += determineLetterValue(charWeCare);
                break;
            }
            if ( len(sack1) eq 0 or len(sack2) eq 0 or len(sack3) eq 0 )
                break;
        }
        return sum;
    }

    function runPart1(filename) {
        var data = FileRead(filename);
        var dataArr = ListToArray(data, CR);
        var sum = dataArr.reduce(
            function(prevVal, thisVal) {                
                return prevVal + calcPriority(thisVal);
            }, 0
        )
        WriteOutput(sum);        
    }

  
   

    function runPart2(filename)
    {   
        var data = FileRead(filename);
        var dataArr = ListToArray(data, CR);
        var sum = 0;
        for ( var i  = 1; i < ArrayLen(dataArr); i+=3)
        {
            sum += calcPart2Priority(dataArr[i], dataArr[i+1], dataArr[i+2]);
    
        }
        writeOutput(sum);
    }

  
        
 
    
}