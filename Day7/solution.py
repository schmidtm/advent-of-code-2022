CR = "\n"
FS = chr(27)

def run_sample() :
    runPart1("SampleInput.txt");
    print("\n")
    runPart2("SampleInput.txt");

def run_realWorld() :
    runPart1("PuzzleInput.txt");
    print("\n")
    runPart2("PuzzleInput.txt");
   
def goUpFolder(folderIn) :
    if folderIn == "/" or folderIn == "" :
        return ""

    arr = folderIn.split("/");
    arr.pop()
    res = "/".join(arr)
    if res == "" :
        res = "/"
    return res

def createFileSystem(dataArr ) :
    folders = {}
    curFolder = "/"
    for line in dataArr :
        lineParts = line.split(" ")
        if lineParts[0] == "$" and lineParts[1] == "cd" :
            folder = lineParts[2]
            if folder == "/" :
                curFolder = "/"
            elif folder == ".." :
                curFolder = goUpFolder(curFolder)
            elif curFolder == "/" :
                curFolder = "/" + folder
            else :
                curFolder = curFolder + "/" + folder
            if curFolder in folders :
                pass
            else :
                if curFolder != "" :
                    folders[curFolder] = { "folderSize":0, "totalFolderSize":0}
        elif lineParts[0] == "$" and lineParts[1] == "ls":
            pass
        elif lineParts[0] == "dir":
            pass
        else :
            fileSize = int(lineParts[0])
            folders[curFolder]["totalFolderSize"] += fileSize            
            folders[curFolder]["folderSize"] += fileSize
            tmpFolder = goUpFolder(curFolder)
            while tmpFolder != "" :
                folders[tmpFolder]["totalFolderSize"] += fileSize
                tmpFolder = goUpFolder(tmpFolder)                
    return folders




def runPart1(fileIn) :
    f = open(fileIn, "r")
    data = f.read()    
    fileSystem = createFileSystem(data.split(CR))
    sizeLookingFor = 100000
    sum = 0
    for i in fileSystem :
        if fileSystem[i]["totalFolderSize"] <= sizeLookingFor :
            sum += fileSystem[i]["totalFolderSize"]
    
    print(sum)


def runPart2(fileIn) :
    f = open(fileIn, "r")
    data = f.read()    
    
    fileSystem = createFileSystem(data.split(CR))
    fileSystemSize = 70000000
    unusedSpaceNeeded = 30000000
    totalSystemUsed = fileSystem["/"]["totalFolderSize"]
    spaceAvailable = fileSystemSize - totalSystemUsed
    removeThisMuch  = unusedSpaceNeeded - spaceAvailable

    min = fileSystemSize
    for i in fileSystem :
        if fileSystem[i]["totalFolderSize"] >= removeThisMuch :
            if fileSystem[i]["totalFolderSize"] <= min :
                min = fileSystem[i]["totalFolderSize"]
    
    print(min)

print("Sample" + CR)
run_sample()
print (CR + "RealWorld" + CR)
run_realWorld()