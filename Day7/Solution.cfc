component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
         runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
        runPart1("PuzzleInput.txt");
       WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }


    string function goUpFolder(folderIn) {
        if ( folderIn eq "/" )
            return "";

        var ret =  ListDeleteAt(folderIn, ListLen(folderIn, "/"), "/");
        if (ret eq "")
            ret = "/";
        return ret;
    }
    
    function createFileSystem(dataArr) {
        var folders = {
            
        };

        var curFolder = "/";
        for ( var line in dataArr)
        {
            var lineParts = ListToArray(line, " ");
            if (  lineParts[1] eq "$" and lineParts[2] eq "cd" )
            {
                var folder =  lineParts[3];
                if ( folder eq "/" )
                    curFolder = "/";
                else if ( folder eq "..")
                    curFolder = goUpFolder(curFolder);
                else if (curFolder eq "/")
                    curFolder = "/" & folder;
                else
                    curFolder = curFolder & "/" & folder;
                
                if ( not StructKeyExists(folders, curFolder) )
                {
                    folders[curFolder] =  {
                        folderSize:0,
                        totalFolderSize:0
                    };
                }
            } else if ( lineParts[1] eq "$" and  lineParts[2] eq "ls") {

            } else if ( lineParts[1] eq "dir") {
                
            } else {
                var fileSize = lineParts[1];
                
                folders[curFolder].totalFolderSize+=fileSize;
                folders[curFolder].folderSize+=fileSize;
             
                var tmpFolder = goUpFolder(curFolder);
                while ( StructKeyExists(folders, tmpFolder))
                {
                   folders[tmpFolder].totalFolderSize+=fileSize;
                    
                    tmpFolder = goUpFolder(tmpFolder);                    
                }
            }
        }
        return folders;
    }

    function runPart1(filename) {
        var data = FileRead(filename);        
        var dataArr = ListToArray(data, CR);
        var fileSystem = createFileSystem(dataArr);
     
        var onlySmallOnes = structFilter(fileSystem, (key, value) => value.totalFolderSize lte 100000 );
        var res = StructReduce(onlySmallOnes, (result, key, value) => result + value.totalFolderSize,0)
        writeOutput(res);
       
    }

  
   

    function runPart2(filename)
    {   
        var data = FileRead(filename);        
        var dataArr = ListToArray(data, CR);
        var fileSystem = createFileSystem(dataArr);
     
        var fileSystemSize = 70000000;
        var unusedSpaceNeeded = 30000000;
        var totalSystemUsed = fileSystem["/"].totalFolderSize;
        var spaceAvailable = fileSystemSize - totalSystemUsed;
        var removeThisMuch  = unusedSpaceNeeded - spaceAvailable ;
        
        var atLeastThisMuch = structFilter(fileSystem, (key, value) => value.totalFolderSize >= removeThisMuch);
        var res = StructReduce(atLeastThisMuch, (result, key, value) => { if (  value.totalFolderSize <=  result) 
                   return value.totalFolderSize; else return result; }, fileSystemSize);
        
        writeOutput(res);       
       
    }

  
        
 
    
}