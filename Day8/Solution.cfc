component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
         runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
        runPart1("PuzzleInput.txt");
       WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }
/* 
    So if we makea  grid
        Store
            Value
            MaxRight
            MaxLeft
            MaxTop
            MaxBottom

    We start from top left and move down + 1 in all directions
        that way each calculation is determined by the previous
        store maxRight MaxTop heights

    We start from bottom right and thne do the opposite 
*/
    function parseColRows(data) {
        
        rowAr = ListToArray(data, CR);
        Rows = rowAr.map((itm) => {colAr = ListToArray(itm, "");
        return colAr.map((itm) => { return { "Value" : itm, MaxRight:0, MaxLeft:0, MaxTop:0,MaxBottom:0, ScenicScore:0} })
        });
        


       
        return Rows;
    }

    function calcMaxHeights(Rows)
    {
        for ( var j = 2; j lt ArrayLen(Rows); j++)
        {
            for ( var k = 2; k lt ArrayLen(Rows[1]); k++)
            {
                Rows[j][k].MaxLeft = max(Rows[j-1][k].MaxLeft, Rows[j-1][k].Value);
                Rows[j][k].MaxTop = max(Rows[j][k-1].MaxTop, Rows[j][k-1].Value);                
            }
        }

        for ( var j = ArrayLen(Rows) - 1; j gt 1; j--)
        {
            for ( var k = ArrayLen(Rows[1]) - 1; k gt 1 ; k--)
            {
                Rows[j][k].MaxRight = max(Rows[j+1][k].MaxRight, Rows[j+1][k].Value);
                Rows[j][k].MaxBottom = max(Rows[j][k+1].MaxBottom, Rows[j][k+1].Value);                
            }
        }

    }

    function calcScenicScore(Rows) {
        var maxScore = 0;
        var Height = ArrayLen(Rows);
        var Width = ArrayLen(Rows[1])
        for ( var j = 2; j lt Height; j++ )

        {
            for ( var k = 2; k lt Width; k++ )
            {
                tree = Rows[j][k].Value;
                /* travel up till we hit end or hit a tree too big */
                var travelerUp = 1;
                while ( (j-travelerUp) gt 1 and tree gt Rows[j-travelerUp][k].Value )
                {
                    travelerUp++;
                }
                
                var travelerDown = 1;
                while ( (j+travelerDown) lt Height and tree gt Rows[j+travelerDown][k].Value )
                {
                    travelerDown++;
                }

                var travelerLeft = 1;
                while ( (k+travelerLeft) lt Width and tree gt Rows[j][k+travelerLeft].Value )
                {
                    travelerLeft++;
                }

                
                var travelerRight = 1;
                while ( (k-travelerRight) gt 1 and tree gt Rows[j][k-travelerRight].Value )
                {
                    travelerRight++;
                }
                maxScore = max(maxScore, (travelerDown* travelerLeft*travelerRight*travelerUp ))

            }
        }

        return maxScore;
    }

    function countVisible(Rows) 
    {        
        cnt = 0;
        for ( var j = 2; j lt ArrayLen(Rows); j++)
        {
            for ( var k = 2; k lt ArrayLen(Rows[1]); k++)
            {
                var tree = Rows[j][k];
                if ( tree.Value gt tree.MaxLeft 
                    or tree.value gt tree.MaxRight 
                    or tree.value gt tree.MaxBottom 
                    or tree.value gt tree.maxTop)
                    cnt++;
            }
        }
        cnt += ((ArrayLen(Rows)-1 ) * 2) + ((ArrayLen(Rows[1]) -1 ) * 2 ) 
        return cnt;
    }

    function runPart1(filename) {
        var data = FileRead(filename);        
        var Rows = parseColRows(data)
        calcMaxHeights(Rows);
        var res = countVisible(Rows);
       WriteOutput(res);
       
    }

  
   

    function runPart2(filename)
    {   
        var data = FileRead(filename);        
        var dataArr = ListToArray(data, CR);
        var Rows = parseColRows(data)
        var res = calcScenicScore(Rows);
        WriteOutput(res);

    }

  
        
 
    
}