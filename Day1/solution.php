<?php
const CR = '
';
function run_sample() {
    runPart1('SampleInput.txt');
    echo(CR);
    runPart2('SampleInput.txt');
}

function run_realWorld()  {
    runPart1('PuzzleInput.txt');
    echo(CR);runPart2('PuzzleInput.txt');
}

function runPart1($fileIn) {
    $f = fopen($fileIn, 'r');
    $data = fread($f, filesize($fileIn));
    fclose($f);
    $lstData = normalizeData($data);
    $max = -1;
    foreach($lstData as $i) 
    {
        $res = totalCalories($i);
        if ( $res > $max )
            $max = $res;
    }
    echo($max);
}

function runPart2($fileIn) {
    $top3 = [ 0, 0, 0 ];
    $f = fopen($fileIn, 'r');
    $data = fread($f, filesize($fileIn));
    fclose($f);
    $lstData = normalizeData($data);
  
    foreach($lstData as $i)
    {
        $myCals = totalCalories($i);
        if ($myCals > $top3[2] )
        {
            $top3[0] = $top3[1];
            $top3[1] = $top3[2];
            $top3[2] = $myCals;
        } else if ( $myCals > $top3[1]) {
            $top3[0] = $top3[1];
            $top3[1] = $myCals;
        } else if ( $myCals > $top3[0] ) {
            $top3[0] = $myCals;
        }
            
    }
    $sum = $top3[0] + $top3[1] + $top3[2];
    echo($sum);
    
}


function totalCalories($input) {
    $vals = explode(CR, $input);
    $tot = 0;
    foreach ( $vals as $i ) {
        $tot = $tot + (int)$i;
    }
    return $tot;
}

function normalizeData($dataIn) {
    $dataIn = str_replace(' ','',$dataIn);
    $dataIn = str_replace(CR.CR, '|', $dataIn);
    $dataOut = explode('|', $dataIn);
    return $dataOut;
    
}

printf('Sample'.CR);
run_sample();
printf(CR.'RealWorld'.CR);
run_realWorld();
?>