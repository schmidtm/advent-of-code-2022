
$CR = "`n"
function Main
{
    Write-Output "Sample"
    run_sample
    Write-Output "Real World"
    run_realworld
    
}

function run_sample
{
    runPart1 "SampleInput.txt"
    runPart2("SampleInput.txt");
}

function run_realworld
{
    runPart1 "PuzzleInput.txt"
    runPart2("PuzzleInput.txt");
}

function runPart1
{
    [CmdletBinding()]
    param(
        [Parameter()]
        [string]$fileName
    
    )
    $data = Get-Content -Path $fileName -Raw
    $lstData = normalizeData $data
    $max = 0
    foreach($lines in $lstData)
    {
        $res = totalCalories $lines
        if ( $res -gt $max)   
        {
            $max = $res
        }
    }
    Write-Output $max
}
function runPart2
{
    [CmdletBinding()]
    param(
        [Parameter()]
        [string]$fileName
    
    )
    $top3_0 = 0
    $top3_1 = 0
    $top3_2 = 0
    $data = Get-Content -Path $fileName -Raw
    $lstData = normalizeData $data
    foreach($lines in $lstData)
    {
        $res = totalCalories $lines
        if ( $res -gt $top3_2 )
        {
            $top3_0 = $top3_1
            $top3_1 = $top3_2
            $top3_2 = $res
        } else {
            if ( $res -gt $top3_1 ) {
                $top3_0 = $top3_1
                $top3_1 = $res
            } else {
                if ( $res -gt $top3_0 ) {
                    $top3_0 = $res
                }
            }
        }        
    }
    $sum = $top3_0 + $top3_1 + $top3_2 
    Write-Output $sum
    
}
function  totalCalories {
    [CmdletBinding()]
    param(
        [Parameter()]
        [string]$lines
    )
  
    $vals = $lines.Split($CR)
    $tot = 0
    foreach ( $itm in $vals )
    {  
        $tot = $tot + [int]$itm
    }
    return $tot
}

function normalizeData
{
    [CmdletBinding()]
    param(
        [Parameter()]
        [String]$dataIn
    )
    $dataIn = $dataIn.Replace("`r`n", $CR)    
    $dataIn = $dataIn.Replace("`n`r", $CR)
    $dataIn = $dataIn.Replace(" ", "")
    $dataIn = $dataIn.Replace($CR + $CR, "|")
    $dataOut = $dataIn.Split("|")
    return $dataOut
}

Main