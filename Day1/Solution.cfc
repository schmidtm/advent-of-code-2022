component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
        runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
        runPart1("PuzzleInput.txt");
        WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }

    function runPart1(filename) {
        var input = FileRead(filename);
        var arrData = normalizeData(input);
        var max = -1;
        max = arrData.reduce(
            function(prevValue, thisValue)  
            { 
                var res = totalCalories(thisValue);
                if ( res gte prevValue) {
                    return res;
                } else {
                    return prevValue;
                }
            }, max );
        WriteOutput(max);
    }

    function runPart2(filename)
    {   
        var top3 = [ 0, 0, 0]        
        var input = FileRead(filename);
        var arrData = normalizeData(input);
        arrData.each(function(itm) { 
            var myCals = totalCalories(itm);
            if ( myCals gt top3[3] )
            {
                top3[1] = top3[2];
                top3[2] = top3[3];
                top3[3] = myCals;
            } else if ( myCals gt top3[2] ) {
                top3[1] = top3[2];
                top3[2] = myCals;
            } else if ( myCals gt top3[1] ) {
                top3[1] = myCals;
            }
        })
        var max = totalCalories(ArrayToList(top3, CR));
        WriteOutput(max);
    }

    function totalCalories(ListIn)
    {
        var arrVals = ListToArray(ListIn, CR);                
        var res = arrVals.reduce( function(prevValue, thisValue) {
            if ( not IsNUmeric(thisValue) )
                return prevValue;
            return thisValue+prevValue;

        }, 0);
        return res;
    }
        
    array function  normalizeData(dataIn) {
        dataIn = dataIn.replace(" ", "", "ALL");
        dataIn = dataIn.replace(CR & CR, FS, "ALL" );
        var dataOut = ListToArray(dataIn, FS);
        return dataOut;
    }
    
}