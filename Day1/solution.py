CR = "\n"

def run_sample() :
    runPart1("SampleInput.txt");
    print("\n")
    runPart2("SampleInput.txt");

def run_realWorld() :
    runPart1("PuzzleInput.txt");
    print("\n")
    runPart2("PuzzleInput.txt");

def runPart1(fileIn) :
    f = open(fileIn, "r")
    data = f.read()
    lstData = normalizeData(data)
    max = -1
    for i in lstData :
        res = totalCalories(i)
        if  res > max :
            max = res
    print(max)

def runPart2(fileIn) :
    top3 = [ 0, 0, 0 ]
    f = open(fileIn, "r")
    data = f.read()
    lstData = normalizeData(data)
  
    for i in lstData :
        myCals = totalCalories(i)
        if myCals > top3[2] :
            top3[0] = top3[1]
            top3[1] = top3[2]
            top3[2] = myCals
        elif myCals > top3[1] :
            top3[0] = top3[1]
            top3[1] = myCals
        elif myCals > top3[0] : 
            top3[0] = myCals

    sum = top3[0] + top3[1] + top3[2]
    print(sum)  

def totalCalories(input) :
    vals = input.split(CR)
    tot = 0
    for i in vals :
        tot = tot + int(i)
    return tot

def normalizeData(dataIn) :
    dataIn = dataIn.replace("\r\n", CR);
    dataIn = dataIn.replace("\n\r", CR);
    
    dataIn = dataIn.replace(" ", "")
    dataIn = dataIn.replace(CR+CR, "|")
    dataOut = dataIn.split("|")
    return dataOut

print("Sample" + CR)
run_sample()
print (CR + "RealWorld" + CR)
run_realWorld()