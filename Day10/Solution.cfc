component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        WriteOutput("Sample1" & CR);
        runPart1("SampleInput.txt");
        WriteOutput(CR);
        WriteOutput("Sample2" & CR);
        runPart1("SampleInput2.txt");
        WriteOutput(CR);
       // runPart2("SampleInput.txt");
        WriteOutput(CR);
       runPart2("SampleInput2.txt");
    }

    function runRealWorld()
    {
       runPart1("PuzzleInput.txt");
        WriteOutput(CR);
       runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }

    function runCycles(operations)
    {
        opLen={"noop":1,"addx":2};
        var twentyCycleSum=0;
        var begin_xRegister=1;
        var cycleCount=0;
        var cyclesWeCareAbout = [220, 180, 140, 100, 60, 20];
        var pow = operations.reduce(
            (xRegister, thisVal)=>{
              
                val = ListToArray(thisVal," ");
                thisCycleCount = opLen[val[1]];
               
                for ( var i = 1; i <= thisCycleCount; i++ )
                {
                    cycleCount++;
                    if ( cyclesWeCareAbout.len() > 0 && cycleCount eq cyclesWeCareAbout.last() )
                    {
                        cyclesWeCareAbout.pop();
                        twentyCycleSum+=(xRegister*cycleCount);
                    }
                }           
                if (val[1] == "addx" )
                {
                    xRegister+=val[2];
                }
                
                return xRegister;
            }, begin_xRegister
        )

        return twentyCycleSum;
    }
   
   

    function runPart1(filename) {
        var data = FileRead(filename);        
        var operations = ListToArray(data, CR);
        var results = runCycles(operations);
        WriteOutput(results)
      
    }

  
   array function genMatrix(width, height)
   {
    var ret = [];
    for ( var i = 1; i <= height; i++ )
    {
        ret.append(
            ListToArray(repeatString(" ", width), "")
        );
        
    }
    return ret;
   }

   function spriteInPixel(middleOfSprite, pixel)
   {
    pixel = pixel;
        if ( pixel == middleOfSprite || pixel == middleOfSprite-1 ||pixel == middleOfSprite+1)
        {
            return true;
        }
        return false;
   }

    function runCyclesPart2(operations)
    {
        var outMatrix = genMatrix(40, 6);
        opLen={"noop":1,"addx":2};
       var begin_xRegister=2;
        var cycleCount=0;
        var pow = operations.reduce(
            (xRegister, thisVal)=>{
              
                val = ListToArray(thisVal," ");
                thisCycleCount = opLen[val[1]];
               
                
                for ( var i = 1; i <= thisCycleCount; i++ )
                {       
                    cycleCount++;       
                    var row = ceiling(cycleCount / 40);
                    var pixel = ((cycleCount) mod 40);
                    if ( spriteInPixel(xRegister, pixel))
                    {
                        if ( pixel == 0)
                            pixel = 1;
                        outMatrix[row][pixel] = "##";
                    } 
                   
                   
                   
                }           

                if (val[1] == "addx" )
                {
                    xRegister+=val[2];
                }
                
                return xRegister;
            }, begin_xRegister
        )
        return outMatrix;
    }


    function runPart2(filename)
    {   
        var data = FileRead(filename);   
        var operations = ListToArray(data, CR);
        var disp = runCyclesPart2(operations);
       
        lines = disp.map( (line) => line.toList(""));
        WriteOutput(ArrayToList(lines, CR));
    }

  
        
 
    
}