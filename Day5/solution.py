CR = "\n"
FS = chr(27)

def run_sample() :
    runPart1("SampleInput.txt");
    print("\n")
    runPart2("SampleInput.txt");

def run_realWorld() :
    runPart1("PuzzleInput.txt");
    print("\n")
    runPart2("PuzzleInput.txt");
    
def initialLayout(rows, numCols) :
    arrLayout = []
    i = 0
    while ( i < numCols ) :
        arrLayout.append([])
        i+=1

    for itm in rows :
        i=0
        while ( i < numCols ) :
            letPos = (i*4)+1           
            charAtPos = itm[letPos]
            if ( charAtPos != ' ' ) :
                arrLayout[i].insert(0, charAtPos)
            i+=1
    return arrLayout

def runDirections(arrLayout, directions) :
    for dir in directions :
        vals = dir.split(" ")
        numToMove = int(vals[1])
        moveFrom = int(vals[3])-1
        moveTo = int(vals[5])-1
        i = 0
        while ( i < numToMove ): 
            arrLayout[moveTo].append(arrLayout[moveFrom].pop())
            i+=1
       
    return arrLayout

def runMultiMove(arrLayout, directions) :
    for dir in directions :
        vals = dir.split(" ")
        numToMove = int(vals[1])
        moveFrom = int(vals[3])-1
        moveTo = int(vals[5])-1
        
        fromList = "".join(arrLayout[moveFrom])
        toList = "".join(arrLayout[moveTo])
        fromElms = fromList[len(fromList) - numToMove:]
        toList = toList + fromElms
        posToMove = len(fromList) - numToMove
        if posToMove == 0 :
            fromList = ""
        else :
            fromList= fromList[:len(fromList) - numToMove]

        arrLayout[moveFrom] = list(fromList)
        arrLayout[moveTo] = list(toList)        

    return arrLayout 

def runPart1(fileIn) :
    f = open(fileIn, "r")
    data = f.read()    
    dataArr = data.replace(CR + CR, FS).split(FS)
    initPos = dataArr[0].split(CR)
    directions = dataArr[1].split(CR)
    cols = initPos.pop()    
    numCols = int((len(cols)+1)/4)


    arrLayout = initialLayout(initPos, numCols)
    arrLayout = runDirections(arrLayout, directions)
    str = ""
    for  i in arrLayout :
        str += i[len(i)-1]
    
    print(str)

def runPart2(fileIn) :
    f = open(fileIn, "r")
    data = f.read()    
    dataArr = data.replace(CR + CR, FS).split(FS)
    initPos = dataArr[0].split(CR)
    directions = dataArr[1].split(CR)
    cols = initPos.pop()    
    numCols = int((len(cols)+1)/4)


    arrLayout = initialLayout(initPos, numCols)
    arrLayout = runMultiMove(arrLayout, directions)
    str = ""
    
    for  i in arrLayout :
        str += i[len(i)-1]
    
    print(str)
    
    


print("Sample" + CR)
run_sample()
print (CR + "RealWorld" + CR)
run_realWorld()