component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
        runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
          runPart1("PuzzleInput.txt");
        WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }

    function initialLayout(rows, numberColumns)
    {
        var arrLayout = [];
        for ( i = 1; i <= numberColumns; i++)
        {
            ArrayAppend(arrLayout, []);
        }
        rows.each( (itm)  => {
            for ( var i = 1; i <= numberColumns; i++)  
            {
                
                var letPos = (( i -1 ) * 4) + 2;
                var charAtPos = mid(itm, letPOs, 1);
                if ( charAtPos != ' ' )
                {
                    arrayPrepend(arrLayout[i], charAtPos);
                }
            }
        }
        )
        return arrLayout;

    }

    function runDirections(arrLayout, directions) 
    {
        directions.each((dir) => { 
            var vals = ListToArray(dir, " ");
            var numToMove = vals[2];
            var moveFrom = vals[4];
            var moveTo = vals[6];
            for (var i = 1; i lte numToMove; i++)
            {
                ArrayPush(arrLayout[moveTo], ArrayPop(arrLayout[moveFrom]));
            }

        })
    }



    function runMultiMove(arrLayout, directions) {
        directions.each((dir) => { 
            var vals = ListToArray(dir, " ");
            var numToMove = vals[2];
            var moveFrom = vals[4];
            var moveTo = vals[6];
            var fromList = ArrayToList(arrLayout[moveFrom], "")
            var toList = ArrayToList(arrLayout[moveTo], "")
            var fromElms = Right(fromList,numToMove);
            ToList = ToList & fromElms;
            var posToMove = len(fromList) - numToMove;
            if ( posToMove eq 0 )
                fromList = "";
            else            
                fromList= left(fromList, len(fromList) - numToMove);
            arrLayout[moveFrom] = ListToARray(fromList, "");
            arrLayout[moveTo] = ListToARray(ToList, "");
            
            //var movingItms = ArraySlice(arrLayout[moveFrom], ArrayLen(arrLayout[moveFrom]) - numToMove, numToMove);
            //arrLayout[moveTo].addAll(movingItms);
            /*for ( var i = 1; i <= numToMove; i++ )
            {
                arrayPop(arrLayout[moveFrom]);
            }*/
            //ArraySplice(arrLayout[moveTo], ArrayLen(arrLayout[moveTo]), 0,  movingItms);
            //ArraySplice(arrLayout[moveFrom], ArrayLen(arrLayout[moveFrom]) - numToMove, numToMove)
            
        })  
    }

    function runPart1(filename) {
        var data = FileRead(filename);        
        var dataArr = ListToArray(Replace(data, CR & CR, FS), FS);        
        var initPos = ListToArray(dataArr[1], CR);
        var directions = ListToArray(dataArr[2], CR);
        var cols = ArrayPop(initPos);
        var numCols = ArrayLen(ListToArray(cols, " "))        
        var arrLayout = initialLayout(initPos,numCols);
        runDirections(arrLayout, directions);
        var result = ArrayToList(arrLayout.map((itm) => ArrayLast(itm)), "");
        WriteOutput(result);
        
       
    }

  
   

    function runPart2(filename)
    {   
        var data = FileRead(filename);        
        var dataArr = ListToArray(Replace(data, CR & CR, FS), FS);        
        var initPos = ListToArray(dataArr[1], CR);
        var directions = ListToArray(dataArr[2], CR);
        var cols = ArrayPop(initPos);
        var numCols = ArrayLen(ListToArray(cols, " "))        
        var arrLayout = initialLayout(initPos,numCols);
        runMultiMove(arrLayout, directions);
        var result = ArrayToList(arrLayout.map((itm) => ArrayLast(itm)), "");
        WriteOutput(result);
    }

  
        
 
    
}