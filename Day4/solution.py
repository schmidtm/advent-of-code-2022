CR = "\n"

def run_sample() :
    runPart1("SampleInput.txt");
    print("\n")
    runPart2("SampleInput.txt");

def run_realWorld() :
    runPart1("PuzzleInput.txt");
    print("\n")
    runPart2("PuzzleInput.txt");
    
def determineIfOneIsContainedInTheOther(pairs) :
    arrPairs = splitIntoPairs(pairs)
    if arrPairs[0] <= arrPairs[2] and arrPairs[1] >= arrPairs[3] :
        return 1
    if arrPairs[2] <= arrPairs[0] and arrPairs[3] >= arrPairs[1] :
        return 1

    return 0

def determineIntersections(pairs) :
    arrPairs = splitIntoPairs(pairs)
    if arrPairs[1] < arrPairs[2] or arrPairs[3] < arrPairs[0] :
        return 0        
    return 1
    
    
    
def splitIntoPairs(input) :
    outArr = [0,0,0,0]
    arr = input.split(",")
    i=0
    for elm in arr :
        elmArr = elm.split("-")
        for ind in elmArr :
            outArr[i] = int(ind)
            i=i+1

    return outArr



def runPart1(fileIn) :
    f = open(fileIn, "r")
    data = f.read()
    sum = 0
    arr = data.split(CR);
    for i in arr :
        if determineIfOneIsContainedInTheOther(i) :
            sum=sum+1
    
    print(sum)

def runPart2(fileIn) :
    f = open(fileIn, "r")
    data = f.read()
    sum = 0
    arr = data.split(CR);    
    for i in arr :
        if determineIntersections(i) :
            sum=sum+1
    
    print(sum)



print("Sample" + CR)
run_sample()
print (CR + "RealWorld" + CR)
run_realWorld()