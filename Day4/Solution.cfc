component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
        runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
        runPart1("PuzzleInput.txt");
        WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }

    function determineIfOneIsContainedInTheOther(pairs) {
        var arrPairs = splitIntoPairs(pairs);
        if ( arrPairs[1][1] <= arrPairs[2][1] && arrPairs[1][2] >= arrPairs[2][2] )
            return true;
        if ( arrPairs[2][1] <= arrPairs[1][1] && arrPairs[2][2] >= arrPairs[1][2] )
            return true;    

        return false;
    }
    function determineIntersections(pairs) {
        var arrPairs = splitIntoPairs(pairs);
        if (  arrPairs[1][2] < arrPairs[2][1] || arrPairs[2][2] < arrPairs[1][1] )
            return false;
        
        return true;
    }
    
    
    function splitIntoPairs(input) {
        return LIstToArray(input, ",").map( (in) => ListToArray(in, "-"));
    }

    function runPart1(filename) {
        var data = FileRead(filename);
        var dataArr = ListToArray(data, CR);
        var sum = dataArr.reduce(
            function(prevVal, thisVal) {                
               if ( determineIfOneIsContainedInTheOther(thisVal))
               {
                prevVal++;
               }
               return prevVal;
            }, 0
        )
        WriteOutput(sum);        
    }

  
   

    function runPart2(filename)
    {   
        var data = FileRead(filename);
        var dataArr = ListToArray(data, CR);
        var sum = dataArr.reduce(
            function(prevVal, thisVal) {                
               if ( determineIntersections(thisVal))
               {
                prevVal++;
               }
               return prevVal;
            }, 0
        )
        WriteOutput(sum);  
    }

  
        
 
    
}