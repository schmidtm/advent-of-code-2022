component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
        runPart2("SampleInput.txt");
        WriteOutput(CR);
        runPart2("SampleInputPart2.txt");
    }

    function runRealWorld()
    {
       runPart1("PuzzleInput.txt");
        WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }

   
   function traverse(movesIn, numKnots) 
   {
    var results = {
        knots: [],
        tailVisits: {"0_0" : true},
        numKnots: numKnots
    };
    for ( var i = 1; i lte results.numKnots; i++ )
    {
        results.knots[i] = {x:0,y:0};
    }
    var directions = {
        "R" : { X: 0, Y:-1},
        "U" : { X: 1, Y: 0},
        "D" : { X:-1, Y: 0},
        "L" : { X: 0, Y: 1}
    };


    var moveDetails = movesIn.map((a) => { 
        var details = ListToArray(a, " ");     
        var retStr =  {"direction": details[1], "step" : details[2], "grid" : directions[details[1]]};
        return retStr; })
    var direction = "";
    var step = "";
    for ( var move in moveDetails) 
    {
        for ( var i = 1; i lte move.step; i++ )
        {
            results.knots[1].x += move.grid.X;
            results.knots[1].y += move.grid.y;
            adjustNextKnot(results, 2);
            calcTailVisitsKnots(results);
        }
    }

    return results;
   }
   function calcTailVisitsKnots(results) {
        var xPos = results.knots[results.numKnots].x;
        var yPos = results.knots[results.numKnots].y;
        var identifier = xPos & "_" & yPos;
        results.tailVisits[identifier] = true;
    }
   function adjustNextKnot(results, nextKnotNum)
   {
        var dX = results.knots[nextKnotNum-1].x - results.knots[nextKnotNum].x;
        var dY = results.knots[nextKnotNum-1].y - results.knots[nextKnotNum].y;
        var posX = dX.compareTo(0)
        var posY = dY.compareTo(0)
        
        dX = abs(dX);
        dY = abs(dY);

        var xMove = 0;
        var yMove = 0;
        if ( dX <= 1 && dY <= 1 )
        {
            /* no move touching in some direciton */
        }
        else if ( dX == 0 ) {
            yMove = posY;
        } else if ( dY == 0 ) {
            xMove = PosX;
        } else {
            xMove = PosX;
            yMove = posY;
        }
        
        results.knots[nextKnotNum].x += xMove;
        results.knots[nextKnotNum].y += yMove;
        if ( nextKnotNum lt results.numKnots ) 
        {
            adjustNextKnot(results, nextKnotNum+1)
        }
   }

    function runPart1(filename) {
        var data = FileRead(filename);        
        var moves = ListToArray(data, CR);
        var results = traverse(moves, 2);
        WriteOutput(StructCount(results.tailVisits))
      
    }

  
   

    function runPart2(filename)
    {   
        var data = FileRead(filename);   
        var moves = ListToArray(data, CR);
        var results = traverse(moves, 10);
        WriteOutput(StructCount(results.tailVisits))     
        
    }

  
        
 
    
}