component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
         runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
       runPart1("PuzzleInput.txt");
       WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }
    function init()
    {
      
    }

    function isList(pair)
    {
        return left(pair, 1) == '[';
    }
    function trimBrackets(pair) {
        if (isList(pair) )
        {
            pair = mid(pair, 2, len(pair)-2);            
        }
        return pair;
    }
    function extractPairs(pair)
    {
        var Pairs = {};
        var origVal = pair;
        if (isList(pair) )
        {
            pair = mid(pair, 2, len(pair)-2);            
        }
        var pairCount=1;
        var regExp = "\[([^\[\]]+,)*([^\[\]])*\]";
        var res = ReFindNoCase(regExp, pair, 1, true);
        while ( ArrayLen(res.pos) > 1 ) 
        {
            pairs["pair" & pairCount] = mid(pair, res.pos[1], res.len[1]);
            pair = Replace(pair, pairs["pair" & pairCount],"pair" & pairCount, "ALL");
            pairs["pair" & pairCount] = trimBrackets(pairs["pair" & pairCount]);
            pairCount++;
            res = ReFindNoCase(regExp, pair, 1, true);
        }
        
        return {pairs: Pairs, arr:ListToArray(pair), origVal: origVal};
        
    }

    function comparePair(pairs1, pairs2, pairList1, pairList2, ndx) {
     
        var testPair1 = Duplicate(pairs1)
        var testPair2 = Duplicate(pairs2)

        while ( testPair1.len() >  0 and testPair2.len() > 0)
        {
            var item1 = testPair1.shift();
            var item2 = testPair2.shift();
        
            var length = min(testPair1.len(), testPair2.len());       
            
            if ( IsNumeric(item1) && isNumeric(item2))
            {
                if ( item1 > item2 )
                    return -1;
                if ( item1 < item2 )
                    return 1;
            } else if (  not IsNumeric(item1) and not isNumeric(item2)) {
                /* both lists */
                var itmVal = comparePair(ListToArray(pairList1[item1]), ListToArray(pairList2[item2]), pairList1, pairList2  ,ndx );
                if ( itmVal != 0)
                {
                    return itmVal;
                } 
            } else if (IsNumeric(item1) ) {
                var itmVal = comparePair([item1], ListToArray(pairList2[item2]), pairList1, pairList2,ndx   );
                if ( itmVal != 0)
                {
                    return itmVal;
                } 
            } else if (IsNumeric(item2 )) {
                var itmVal = comparePair(ListToArray(pairList1[item1]) ,[item2] , pairList1, pairList2  ,ndx );
                if ( itmVal != 0)
                {
                    return itmVal;
                }
            }            
        }
            

        if (testPair1.len() > 0 )
            return -1;
        
        if ( testPair2.len() > 0 )
            return 1;
        
        
        
        return 0;
    }

    function runPart1(filename) {
        var data = FileRead(filename);        
        var pairs = ListToArray(data, CR&CR, true, true);
        
        var pairSet = pairs.map( (pair) => ArraySlice(pair.split("\R"), 1))
        var sum = 0;
        for ( var i = 1; i <= ArrayLen(pairSet); i++)
        {            
            var pair = pairSet[i];
            pair1 = extractPairs(pair[1]);
            pair2 = extractPairs(pair[2]);
            if ( comparePair(pair1.arr, pair2.arr, pair1.pairs, pair2.pairs, i) gt 0)
            {
                sum+=i;
            }
        }
       
        WriteOutput(sum);
    }

    function compPair(pair1, pair2) 
    {
        var res = comparePair(pair1.arr, pair2.arr, pair1.pairs, pair2.pairs, 0);
        
        if ( res == 1 )
            return -1;
        else if ( res == -1 )
            return 1;
        return 0;
    }
   

    function runPart2(filename)
    {   
        var data = FileRead(filename);        
        data = data & "#CR#[[2]]#CR#[[6]]"
        var pairs = ListToArray(data, CR, false, true);
        var extraPairs = pairs.map((pair) => extractPairs(pair));
        extraPairs.sort(compPair)
        var finalPairs = extraPairs.map((pair) => pair.origVal)
        var indx1 = finalPairs.indexOf("[[2]]")+1;
        var indx2 = finalPairs.indexOf("[[6]]")+1;
        WriteOutput("#indx1# * #indx2# = #indx1*indx2#")
        
    }

  
        
 
    
}