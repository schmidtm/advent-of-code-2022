component {
	CR = chr(10);
	FS = chr(27);
	function determineCR()
	{
	   
	}
	function runSample()
	{
		WriteOutput("Sample1" & CR);
		runPart1("SampleInput.txt");
		WriteOutput(CR);
		runPart2("SampleInput.txt");
		WriteOutput(CR);
	  
	}

	function runRealWorld()
	{
	   runPart1("PuzzleInput.txt");
	   WriteOutput(CR);
	   runPart2("PuzzleInput.txt");
		
	}
	function init()
	{
	   CR = CreateObject("java","java.lang.System").getProperty("line.separator");
	} 
   
	function parseMonkey(monkeyLines)
	{
	   var monkeyData = monkeyLines.ListToArray(CR);
	   var monkey = {
		// we should have six lines 
			items : monkeyData[2].ReplaceNoCase("  Starting items: ", "").ReplaceNoCase(" ", "", "ALL").ListToArray(),
			Op: monkeyData[3].ReplaceNoCase("  Operation: new = ", ""),
			itemsInspected:  0,
			test : {
				divisible : monkeyData[4].ReplaceNoCase("  Test: divisible by ", ""),
				trueResult : monkeyData[5].ReplaceNoCase("    If true: throw to monkey ", ""),
				FalseResult : monkeyData[6].ReplaceNoCase("    If false: throw to monkey ", "")
			}         
		}
		
		for ( var i = 1; i lte ArrayLen(monkey.items) ;i++)
		{
			monkey.items[i] = monkey.items[i];
		
		}
		return monkey;

	}

	function runRound(monkeys, worryDivider=3,superMod=1) {
		for ( monkey in monkeys ) {
			while ( monkey.items.len() gt 0 )
			{
				monkey.itemsInspected++;
				var thisItem = monkey.items.pop();
				thisItem = runOp(thisItem, monkey.Op);
				thisItem = thisItem mod superMod;b
				if ( worryDivider gt 1)
					thisItem = int(thisItem / worryDivider);
				if ( thisItem mod monkey.test.divisible == 0)
				{
					monkeys[monkey.test.trueResult+1].items.push(thisItem);
				} else {
					monkeys[monkey.test.falseResult+1].items.push(thisItem);
				}
			}
		}
	}

	function runOp(val, Op)
	{        
		op = op.replace(" ", "", "ALL");
		if ( op contains "old*old" )
		{
			return val * val;
		} else if ( op contains "*") {
			op = op.replace("old*", "");
			return val * op;
		} else if ( op contains "old+old") {
			return val + val
		} else {
			op = op.replace("old+", "");
			return val + op;
		}        
	}

	function dumpItemsOnly(monkeys) {
		var i = 0;
		for ( monkey in monkeys )
		{
			WriteOutput("Monkey #i# #serializeJSON(monkey.Items)# #CR#");
			i++;
		}
	}

	
	
	function findMax2(monkeys)
	{
		monkeys.sort( (monkey1, monkey2) => {
			if ( monkey1.itemsInspected < monkey2.itemsInspected)
				return -1
			else if ( monkey1.itemsInspected > monkey2.itemsInspected)
				return 1;
			else
				return 0;
		})
		return {first:monkeys[monkeys.len()].itemsInspected, second:monkeys[monkeys.len()-1].itemsInspected};				
	}

	function getSuperMod(monkeys) {
		return monkeys.reduce((prevVal, monkey) => prevVal*monkey.test.divisible, 1);
	}
	
	function runPartX(filename, numRounds, worryDividider) {
		var data = FileRead(filename);
		var monkeysData = ListToArray(data, CR & CR, false, true);

		var monkeys = monkeysData.map((monkey) => parseMonkey(monkey) );
		var superMod = getSuperMod(monkeys);
		for ( var i = 1; i lte numRounds; i++) 
		{
			runRound(monkeys, worryDividider, superMod);
		}        
		dumpItemsOnly(monkeys, false, "text");
		
		var maxes = findMax2(monkeys);
		WriteOutput(maxes.first * maxes.second & CR);
	}
	
	function runPart1(fileName)
	{
		runPartX(fileName, 20, 3);
	}
	function runPart2(fileName) {
		runPartX(fileName, 10000, 1);
	}
	
	
}