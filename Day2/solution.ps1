
$CR = "`n"
$PointValues  = @{'R'= 1; 'P'= 2; 'S' = 3}
$TheirSide = @{'A' = 'R'; 'B' = 'P'; 'C' = 'S'}
$Combos = @{
    'RP' = 6; 'PS' = 6; 'SR'  = 6; 
    'RR' = 3; 'SS' = 3; 'PP' = 3;
    'PR' =0; 'SP' = 0; 'RS' = 0}

function Main
{
    Write-Output "Sample"
    run_sample
    Write-Output "Real World"
    run_realworld
    
}

function run_sample
{
    runPart1 "SampleInput.txt"
   runPart2("SampleInput.txt");
}

function run_realworld
{
    runPart1 "PuzzleInput.txt"
    runPart2("PuzzleInput.txt");
}

function runPart1
{
    [CmdletBinding()]
    param(
        [Parameter()]
        [string]$fileName
    
    )
    
    $MySide = @{'X' = 'R'; 'Y' = 'P'; 'Z' = 'S'}
    $data = Get-Content -Path $fileName -Raw
    $lstData = normalizeData $data
    $sum = 0
    foreach($lines in $lstData)
    {
        $hands = $lines.Split(" ")
        $theirHand = $TheirSide[$hands[0]]
        $myHand = $MySide[$hands[1]]
        $thisScore = calcScore $theirHand $myHand
        $sum = $sum + $thisScore
        
    }
    Write-Output $sum
}

function calcScore {
       [CmdletBinding()]
    param(
        [Parameter()]
        [string]$TheirSide,
        
        [Parameter()]
        [string]$MySide
    
    )
    return $PointValues[$MySide] + $Combos[$TheirSide+$MySide]
}

function runPart2
{
    [CmdletBinding()]
    param(
        [Parameter()]
        [string]$fileName
    
    )
    $OutCome = @{'X' = 'LOSS'; 'Y' = 'DRAW'; 'Z' = 'WIN'}
    $MyWin = @{'R' = 'P'; 'S' = 'R'; 'P' = 'S'}    
    $MyLoss = @{'P' = 'R'; 'R' = 'S'; 'S' = 'P'}
    $data = Get-Content -Path $fileName -Raw
    $lstData = normalizeData $data
    $sum = 0
    foreach($lines in $lstData)
    {
        $hands = $lines.Split(" ")
        $theirHand = $TheirSide[$hands[0]]
        $result = $OutCome[$hands[1]]
        $myHand = $theirHand
        if ($result -eq "LOSS" )
        {
            $myHand = $MyLoss[$theirHand]

        } else {
            if ( $result -eq "WIN")
            {
                $myHand = $MyWin[$theirHand]
            }
        }

        $thisScore = calcScore $theirHand $myHand
        $sum = $sum + $thisScore
        
    }
    Write-Output $sum
    
}
function  totalCalories {
    [CmdletBinding()]
    param(
        [Parameter()]
        [string]$lines
    )
  
    $vals = $lines.Split($CR)
    $tot = 0
    foreach ( $itm in $vals )
    {  
        $tot = $tot + [int]$itm
    }
    return $tot
}

function normalizeData
{
    [CmdletBinding()]
    param(
        [Parameter()]
        [String]$dataIn
    )
    $dataIn = $dataIn.Replace("`r`n", $CR)    
    $dataIn = $dataIn.Replace("`n`r", $CR)
    #$dataIn = $dataIn.Replace(" ", "")    
    $dataOut = $dataIn.Split($CR)
    return $dataOut
}

Main