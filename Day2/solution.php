<?php
const CR = '
';
const PointValues  = array('R' => 1, 'P' => 2, 'S' => 3);
const TheirSide = array('A' => 'R', 'B' => 'P', 'C' => 'S');
const Combos = array(
    'RP' => 6, 'PS' => 6, 'SR'  => 6, 
    'RR' => 3, 'SS' => 3, 'PP' => 3,
    'PR' => 0, 'SP' => 0, 'RS' => 0);



function run_sample() {
    runPart1('SampleInput.txt');
    echo(CR);
    runPart2('SampleInput.txt');
}

function run_realWorld()  {
    runPart1('PuzzleInput.txt');
    echo(CR);
    runPart2('PuzzleInput.txt');
}

function runPart1($fileIn) {
    $MySide = array('X' => 'R', 'Y' => 'P', 'Z' => 'S');
     
    $f = fopen($fileIn, 'r');
    $data = fread($f, filesize($fileIn));
    fclose($f);
    $sum = 0;
    $dataArr = explode(CR, $data);
    foreach($dataArr as $i) 
    {
        $hands = explode(" ", $i);
        $theirHand = TheirSide[$hands[0]];
        $myHand = $MySide[$hands[1]];
        $sum = $sum + calcScore($theirHand, $myHand);        
    }
    echo($sum);
}

function calcScore($theirSide, $MySide)
{
    return PointValues[$MySide] + Combos[$theirSide.$MySide];
}

function runPart2($fileIn) {
    $OutCome=array("X" => "LOSS", "Y" => "DRAW", "Z" => "WIN");
    $MyWin = array("R" => "P", "S" => "R", "P" => "S");
    $MyLoss = array("P" => "R", "R" => "S", "S" => "P");
    $f = fopen($fileIn, 'r');
    $data = fread($f, filesize($fileIn));
    fclose($f);  $sum = 0;
    $dataArr = explode(CR, $data);
    foreach($dataArr as $i) 
    {
        $hands = explode(" ", $i);
        $theirHand = TheirSide[$hands[0]];
        $myHand = $theirHand;
        $result = $OutCome[$hands[1]];
        if ( $result == "LOSS")
        {
            $myHand = $MyLoss[$theirHand];
        } else if ($result == "WIN" ) {
            $myHand = $MyWin[$theirHand];
        }
        $sum = $sum + calcScore($theirHand, $myHand);        
    }
    echo($sum);
}


printf('Sample'.CR);
run_sample();
printf(CR.'RealWorld'.CR);
run_realWorld();
?>