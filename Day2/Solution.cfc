component {
    CR = "
";
    FS = chr(27);
    function runSample()
    {
        runPart1("SampleInput.txt");
        WriteOutput(CR);
        runPart2("SampleInput.txt");
    }

    function runRealWorld()
    {
        runPart1("PuzzleInput.txt");
        WriteOutput(CR);
        runPart2("PuzzleInput.txt");
        
    }

    function runPart1(filename) {
        var TheirSide = {"R" = "A", "P"="B", "S"="C"};
        var MySide = {"R"="X", "P"="Y", "S"="Z"};
        var revMySide =  {};
        var revTheirSide = {};
        TheirSide.each(function(key, value) {revTheirSide[value]=key});
        MySide.each(function(key, value) {revMySide[value]=key});
        this.points = {"R"=1, "P"=2, "S"=3};
        var data = FileRead(filename);
        var dataArr = ListToArray(data, CR);
        var sum = dataArr.reduce(
            function(prevVal, thisVal) {
                var itms = ListToArray(thisVal, " ");                
                return prevVal + calcScore(revTheirSide[itms[1]], revMySide[itms[2]]);
            }, 0
        )
        WriteOutput(sum);
    }

    numeric function determinePoints(MySide)
    {
        return this.points[MySide];
    }

    numeric function calcScore(TheirSide, MySide) 
    {
        var adder = determinePoints(MySide);
        if ( TheirSide eq MySide )
            return adder+3;
        if ( ( TheirSide eq "R" and MySide eq "P") 
            OR (TheirSide eq "P" and MySide eq "S")
            OR (TheirSide eq "S" and MySide eq "R") )
        {
            return adder+6;
        } else {
            return adder+0;
        }
    }

    function runPart2(filename)
    {   
        var TheirSide = {"R" = "A", "P"="B", "S"="C"};
        var OUTCOME = {"X"="LOSS", "Y"="DRAW", "Z" = "WIN"};
        var MyWin = { "R" = "P", "S" = "R", "P" = "S"};
        var MyLoss = {};
        MyWin.each(function(key, value) {MyLoss[value]=key});
        var revOUTCOME = {};
        OUTCOME.each(function(key, value) {revOUTCOME[value]=key});
        var revTheirSide = {};
        TheirSide.each(function(key, value) {revTheirSide[value]=key});
        this.points = {"R"=1, "P"=2, "S"=3};

        var data = FileRead(filename);
        var dataArr = ListToArray(data, CR);
        var sum = dataArr.reduce(
            function(prevVal, thisVal) {
                var itms = ListToArray(thisVal, " ");
                var myHand = "";
                var theirHand = revTheirSide[itms[1]];
                if ( itms[2] eq revOUTCOME["DRAW"] )
                {
                    myHand = theirHand;
                } else if (itms[2] eq revOUTCOME["LOSS"]) {
                    myhand = myLoss[theirHand];
                } else if ( itms[2] eq revOUTCOME["WIN"])
                {
                    myHand = myWin[theirHand];
                }
                return prevVal + calcScore(theirHand, myHand);
            }, 0
        )
        WriteOutput(sum);
    }

  
        
 
    
}