import os

PointValues  = {'R': 1, 'P': 2, 'S' : 3}
TheirSide = {'A' : 'R', 'B' : 'P', 'C' : 'S'}
Combos = {
    'RP' : 6, 'PS' : 6, 'SR'  : 6, 
    'RR' : 3, 'SS' : 3, 'PP' : 3,
    'PR' :0, 'SP' : 0, 'RS' : 0}
CR = '\n'
def run_sample() :
    runPart1("SampleInput.txt");
    print("\n")
    runPart2("SampleInput.txt");

def run_realWorld() :
    runPart1("PuzzleInput.txt");
    print("\n")
    runPart2("PuzzleInput.txt");

def runPart1(fileIn) :
    MySide = {'X' : 'R', 'Y': 'P', 'Z' :'S'}    
    f = open(fileIn, "r")
    data = f.read()
    dataArr = data.split(CR)

    sum = 0
    for i in dataArr :
        i = i.strip()
        hands = i.split(" ")
        theirHand = TheirSide[hands[0]]
        myHand = MySide[hands[1]]
        sum = sum + calcScore(theirHand, myHand)

    print(sum)

def runPart2(fileIn) :
    OutCome={"X" : "LOSS", "Y" : "DRAW", "Z" : "WIN"}
    MyWin = {"R": "P", "S":"R", "P":"S"}
    MyLoss = {"P": "R", "R":"S", "S":"P"}
    f = open(fileIn, "r")
    data = f.read()
    dataArr = data.split(CR)
    sum = 0
    for i in dataArr :
        i = i.strip()
        hands = i.split(" ")
        result = OutCome[hands[1]];
        theirHand = TheirSide[hands[0]]
        myHand = theirHand
        if result == "LOSS" :
            myHand = MyLoss[theirHand]
        elif result == "WIN" :
            myHand = MyWin[theirHand]
        sum = sum + calcScore(theirHand, myHand)
    print(sum)

def calcScore(TheirSide, MySide) :
    return PointValues[MySide] + Combos[TheirSide + MySide]


print("Sample" + CR)
run_sample()
print ("RealWorld" + CR)
run_realWorld()